import yfinance as yf # yfinance gets data comes from CoinMarketCap
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import os
import datetime

# TODO replace slashes with os.path.join ...
DATA_FOLDER = 'data/latest/crypto prices'
YFINANCE_WATCHLIST_PATH = f'{DATA_FOLDER}/yfinance/meta/watchlist.csv'
YFINANCE_BTC_USD_EPOCH = '2014-09-17' # When BTC price starts on yfinance
YFINANCE_CRYPTO_SITE = 'https://finance.yahoo.com/cryptocurrencies/?offset=0&count=100'

def get_watchlist(src='yfinance') -> list:
    ''' Returns list of tickers being watched.'''
    
    watchlist = pd.read_csv(YFINANCE_WATCHLIST_PATH)
    return list(watchlist['ticker'])


def check_ticker_exists(ticker):
    
    if yf.Ticker(ticker).info['regularMarketPrice']==None:
        raise Exception(f'Ticker not found ({ticker}). Check {YFINANCE_CRYPTO_SITE}')

    pass

        
def add_ticker(ticker:str) -> list:
    ''' Add ticker to watchlist '''
    
    watchlist = pd.read_csv(YFINANCE_WATCHLIST_PATH)
    
    if ticker in watchlist['ticker'].values:
        return list(watchlist['ticker'])
    
    check_ticker_exists(ticker)

    watchlist.loc[len(watchlist.index)] = ticker
    watchlist.to_csv(YFINANCE_WATCHLIST_PATH)

    refresh_price_history(single_names=[])
    return list(watchlist['ticker'])


def refresh_price_history(src='yfinance', single_names=[]):
    '''Cache max history available. Can take a while.'''
    
    tickers = single_names
    if single_names ==[]:
        tickers = get_watchlist(src)
    
    # TODO parallelize this & add tqdm
    for ticker in tickers: 
        check_ticker_exists(ticker)
        df = yf.Ticker(ticker).history(period='max')
        df['pct_change'] = df['Close'].pct_change() 
        df['log_pct_change'] = df['Close'].pct_change().apply(lambda x: np.log(1+x))   
        df.to_csv(f'{DATA_FOLDER}/{src}/{ticker}_history.csv')

        
def get_raw_data(ticker, num_days=5, src='yfinance'):
    prices = pd.read_csv(f'{DATA_FOLDER}/{src}/{ticker}_history.csv', index_col='Date')
    display(prices.tail(num_days))
    
        
def get_cached_log_returns(src='yfinance'):
    df = pd.DataFrame()
    df.index = pd.date_range(start=YFINANCE_BTC_USD_EPOCH, end=datetime.datetime.now().date().isoformat(),freq='D')
    df.index.name='Date'
    
    for file in os.listdir(f'{DATA_FOLDER}/{src}'):
        if file.endswith('csv'):
            ticker = file.split('_')[0]
            prices = pd.read_csv(f'{DATA_FOLDER}/{src}/{file}', index_col='Date')
            prices.index = pd.to_datetime(prices.index)
            prices[ticker] = prices['log_pct_change']            
            df = df.join(prices[ticker])
            
    return df


def get_corr_matrix(tickers:list, num_days=90):
    all_log_returns = get_cached_log_returns()
    display(all_log_returns[tickers].corr())

    
def get_cov_matrix(tickers:list, num_days=90):
    all_log_returns = get_cached_log_returns()
    display(all_log_returns[tickers].cov())

    
def run_opt(rdf, assets, num_days=200, runs=2000):

    odf = rdf[assets].tail(num_days)
    mean_log_returns = odf.mean()
    cov_log_returns = odf.cov()

    num_tickers = len(assets)
    p_returns = []
    p_volatility = []
    sample_portfolio = {}

    for x in range(runs):
        weights = np.random.random(num_tickers)
        weights /= np.sum(weights)
        sample_portfolio[x] = dict(zip(assets,weights))

        returns = np.sum(weights * mean_log_returns)
        sample_portfolio[x]['returns'] = returns

        volatility = np.sqrt(np.dot(weights.T, np.dot(cov_log_returns, weights)))
        sample_portfolio[x]['volatility'] = volatility
        sample_portfolio[x]['sharpe'] = returns / volatility

        p_returns.append(returns)
        p_volatility.append(volatility)

    p_returns = np.array(p_returns)
    p_volatility = np.array(p_volatility)
    portfolios = pd.DataFrame({"Return": p_returns, 'Volatility': p_volatility})
    portfolios.plot(x="Volatility", y="Return", kind="scatter", figsize=(10, 6))
    plt.title(f"Efficient Frontier {assets}")
    plt.xlabel("Volatility")
    plt.ylabel("Return")
    plt.show()
    
    return pd.DataFrame.from_dict(sample_portfolio,orient='index')